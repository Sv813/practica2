const h1 = document.querySelector('h1');
const p = document.querySelector('p');
const parrafoclase = document.querySelector('.parrafoclase');
const pid = document.querySelector('#pid');
const input = document.querySelectorAll('input')
const btnE = document.querySelector('#btnEnviar');
const btnL = document.querySelector('#btnLimpiar');
const form = document.querySelector('form')
let Lista = document.querySelector('#lista')

//Lista.innerHTML = ''

function estudiante(nombre, apellido, curso) {
    return { nombre, apellido, curso }
}
function capturarDatos() {
    let nom, app, curso
    nom = input[0].value
    app = input[1].value
    curso = input[2].value
    console.log(estudiante(nom, app, curso))
    let tr = document.createElement('tr')
    tr.innerHTML = `
    <td>${nom}</td>
    <td>${app}</td>
    <td>${curso}</td>
    `
    Lista.append(tr)

    input[0].value = ""
    input[1].value = ""
    input[2].value = ""
}

function limpiarTabla() {
    Lista.innerHTML = ''
}

form.addEventListener('submit', (e) => {
    e.preventDefault()
})
btnE.addEventListener("click", function () {
    capturarDatos();
})
btnL.addEventListener("click", function () {
    limpiarTabla();
})